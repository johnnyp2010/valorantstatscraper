from models.personal_stats import PersonalStats
from selenium import webdriver


def retrieve_top_level_stat(driver, class_name):
    return driver.find_element("xpath", f"//span[@class='{class_name}']").text


def retrieve_lower_level_stat(driver, title):
    return driver.find_element("xpath", f"//span[@title='{title}']/following-sibling::span").text




def create_player_stats(player, driver):
    driver.get(player.url)

    playtime = retrieve_top_level_stat(
        driver, "playtime").replace("h Play Time", "")
    matches = retrieve_top_level_stat(
        driver, "matches").replace(" Matches", "").replace(",","")
    damage_per_round = retrieve_lower_level_stat(driver, "Damage/Round")
    kd_ratio = retrieve_lower_level_stat(driver, "K/D Ratio")
    headshot_percentage = retrieve_lower_level_stat(
        driver, "Headshot%").replace("%", "")
    win_percentage = retrieve_lower_level_stat(
        driver, "Win %").replace("%", "")
    wins = retrieve_lower_level_stat(driver, "Wins").replace(",","")
    kills = retrieve_lower_level_stat(driver, "Kills").replace(",","")
    deaths = retrieve_lower_level_stat(driver, "Deaths").replace(",","")
    assists = retrieve_lower_level_stat(driver, "Assists").replace(",","")
    score_per_round = retrieve_lower_level_stat(driver, "Score/Round")
    kad_ratio = retrieve_lower_level_stat(driver, "KAD Ratio")
    kills_per_round = retrieve_lower_level_stat(driver, "Kills/Round")
    plants = retrieve_lower_level_stat(driver, "Plants").replace(",","")
    first_bloods = retrieve_lower_level_stat(driver, "First Bloods")
    clutches = retrieve_lower_level_stat(driver, "Clutches").replace(",","")
    flawless = retrieve_lower_level_stat(driver, "Flawless").replace(",","")
    aces = retrieve_lower_level_stat(driver, "Aces")

    return PersonalStats(
        player.name,
        int(playtime),
        int(matches),
        float(damage_per_round),
        float(kd_ratio),
        float(headshot_percentage),
        float(win_percentage),
        int(wins),
        int(kills),
        int(deaths),
        int(assists),
        float(score_per_round),
        float(kad_ratio),
        float(kills_per_round),
        int(plants),
        int(first_bloods),
        int(clutches),
        int(flawless),
        int(aces))
    
def get_all_stats(players):
    driver = webdriver.Chrome()
    personal_stats = []
    for player in players:
        personal_stats.append(create_player_stats(player, driver))
    driver.close()
    return personal_stats