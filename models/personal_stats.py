import json


class PersonalStats:
    def __init__(self, 
    name, 
    playtime, 
    matches, 
    damage_per_round,
    kd_ratio,
    headshot_percentage,
    win_percentage,
    wins,
    kills,
    deaths,
    assists,
    score_per_round,
    kad_ratio,
    kills_per_round,
    plants,
    first_bloods,
    clutches,
    flawless,
    aces):
        self.name = name
        self.playtime = playtime
        self.matches = matches
        self.damage_per_round = damage_per_round
        self.kd_ratio = kd_ratio
        self.headshot_percentage = headshot_percentage
        self.win_percentage = win_percentage
        self.wins = wins
        self.kills = kills
        self.deaths = deaths
        self.assists = assists
        self.score_per_round = score_per_round
        self.kad_ratio = kad_ratio
        self.kills_per_round = kills_per_round
        self.plants = plants
        self.first_bloods = first_bloods
        self.clutches = clutches
        self.flawless = flawless
        self.aces = aces

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)

    def to_array(self):
        return [self.name, 
        self.playtime, 
        self.matches,
        self.damage_per_round, 
        self.kd_ratio, 
        self.headshot_percentage,
        self.win_percentage, 
        self.wins, 
        self.kills,
        self.deaths,
        self.assists,
        self.score_per_round,
        self.kad_ratio,
        self.kills_per_round,
        self.plants,
        self.first_bloods,
        self.clutches, 
        self.flawless,
        self.aces]
