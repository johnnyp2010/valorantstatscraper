class Player:
    def __init__(self, name, base_url, suffix, username):
        self.name = name
        self.base_url = base_url,
        self.suffix = suffix,
        self.username = username
        self.url = f"{base_url}{username}{suffix}"