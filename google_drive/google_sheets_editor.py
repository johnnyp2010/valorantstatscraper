from datetime import datetime
import gspread


def create_worksheet(spreadsheet_name):
    service_account = gspread.service_account()
    sheet = service_account.open(spreadsheet_name)

    now = datetime.now()
    sheet_name = now.strftime("%Y-%m-%d %H:%M:%S")
    print(f"Creating sheet: {sheet_name}")
    return sheet.add_worksheet(title=sheet_name, rows=100, cols=20)


def write_row(row_number, cell_values, worksheet):
    range = f"A{row_number}:S{row_number}"
    cell_list = worksheet.range(range)

    for i, val in enumerate(cell_values):  # gives us a tuple of an index and value
        # use the index on cell_list and the val from cell_values
        cell_list[i].value = val

    worksheet.update_cells(cell_list)


def write_header(worksheet):
    print(f"Inserting header")
    cell_values = ["Player", "Play Time (hr)", "Matches", "Damage/Round",
                   "K/D Ratio", "Headshot %", "Win %", "Wins", "Kills", "Deaths",
                   "Assists", "Score/Round", "KAD Ratio", "Kills/Round", "Plants",
                   "First Bloods", "Clutches", "Flawless", "Aces"]

    write_row(1, cell_values, worksheet)


def write_personal_stats(personal_stats, worksheet):
    row_number = 2
    for stat in personal_stats:
        print(f"Writing stats for {stat.name}")
        cell_values = stat.to_array()
        write_row(row_number, cell_values, worksheet)
        row_number += 1


def format_sheet(worksheet):
    print('Formatting sheet')
    worksheet.format('A1:S1', {'textFormat': {'bold': True}})
    worksheet.freeze(cols=1)


def publish_stats(personal_stats, spreadsheet_name):
    worksheet = create_worksheet(spreadsheet_name)
    write_header(worksheet)
    write_personal_stats(personal_stats, worksheet)
    format_sheet(worksheet)
