# Valorant Stat Scraper

## Dependencies
* `Google Chrome`
* `Python`
* `pip`

## Set Up
1. Clone the repo
2. Install Selenium with  `pip install selenium webdriver-manager`
3. Install gspread with  `pip install gspread`
4. Create a Service Account in Google and copy the JSON key to the correct location per the instructions [here.](https://docs.gspread.org/en/latest/oauth2.html)


## Running the script

1. From the root of the repository, run `python main.py {Google Sheet Name}` such as `python main.py ValorantStats`