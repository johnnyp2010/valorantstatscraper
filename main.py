from google_drive.google_sheets_editor import publish_stats
from models.player import Player
from personal_stat_creator import get_all_stats
import sys


players = []
base_url = "https://tracker.gg/valorant/profile/riot/"
suffix = "/overview"
players.append(Player("Kam", base_url, suffix, "Kamst3r%232022"))
players.append(Player("TJ", base_url, suffix, "Spartan117Echo%23NA1"))
players.append(Player("Dylan", base_url, suffix, "APrettyBigDyl%233539"))
players.append(Player("Drea", base_url, suffix, "cokeslurpee%238354"))
players.append(Player("Jeremy", base_url, suffix, "DerpSlurp%23UWU"))

personal_stats = get_all_stats(players)

if len(sys.argv) < 2:
    print("You must call with the name of the Google Sheet, such as 'python main.py ValorantStats'")
    quit(1)

spreadsheet_name = sys.argv[1]
publish_stats(personal_stats, spreadsheet_name)